package org.structures.array.listeners;

/**
 *
 * @author martin
 */
@FunctionalInterface
public interface CapacityListener {
    public void checkCapacity();
}
